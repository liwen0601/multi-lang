//===- Collection.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "Hello World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Pass.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm-c/Core.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

#include <iostream>
#include <map>
#include <set>
#include <string>

/**********************************************************************
add static instrumentation and run with dynamic library
***********************************************************************/

using namespace llvm;
using namespace std;

namespace llvm 
{   
    struct Instrument
	{
        int m_iVarNo;
        Module *m_pModule;
        map<Value*, string> m_valToNameMap;
        string strMsg;

        string getValueName(Value* Val)
		{
            string strName = "%";
            
			if (Val->getName().empty())
			{               
                map<Value*, string>::iterator itVal = m_valToNameMap.find(Val);

                if (itVal == m_valToNameMap.end())
                {
                    strName += to_string(m_iVarNo);
                    m_valToNameMap.insert(pair<Value*, string>(Val, strName)); 
				    m_iVarNo++;                
                }
                else
                {
			        strName = itVal->second;
                }
			}
			else
            {
				strName = Val->getName().str();
			}
            
			return strName;
		}

        Constant *geCollectFunction() 
        {
            LLVMContext &context = m_pModule->getContext();

            /* void CollectionInfo (char *pData) */       
            Type *ArgTypes[] = {Type::getInt8PtrTy(m_pModule->getContext())};
            FunctionType *fCollectionInfoTy = FunctionType::get(Type::getVoidTy(context), ArgTypes, false);
            
            return m_pModule->getOrInsertFunction("CollectionInfo", fCollectionInfoTy);
        }

        void AddInstrumentation (Instruction& inst, string CollectInfo)
        {
            Value *CollectFunc    = geCollectFunction ();
            IRBuilder<> Builder(&inst);
                
            Value *vParaVal = Builder.CreateGlobalStringPtr(CollectInfo, "");
            Builder.CreateCall(CollectFunc, {vParaVal});

            return;
        }

        void addMsgField(string strField)
    	{
    		strMsg += "{" + strField + "}";
    	}

        void processLoad (Instruction* Inst)
        {
            /* add operand to the node */ 
            LoadInst* linst = dyn_cast<LoadInst>(Inst);
            
            string strLdVal = getValueName(linst->getPointerOperand());

            //errs()<<strLdVal<<"\r\n";
            addMsgField("use:" + strLdVal);
            
            return;
        }

        void processStore (Instruction* Inst)
        {
            StoreInst* sinst = dyn_cast<StoreInst>(Inst);

            string strVal     = getValueName(sinst->getValueOperand());
            string strPointer = getValueName(sinst->getPointerOperand());

            addMsgField("define:" + strPointer);
            addMsgField("use:" + strVal);
            //errs()<<"define variable: "<< strPointer << " from " << strVal <<"\r\n";
            
            return;
        }

        void processDefault (Instruction* Inst)
        {
            //errs()<<"use variable: \r\n";
            for (Instruction::op_iterator op = Inst->op_begin(), opEnd = Inst->op_end(); op != opEnd; ++op)
			{
				if (auto opInst = dyn_cast<Instruction>(*op))
				{
                     processInst(opInst);
				}
                else
                {
                    string strOpName = getValueName(*op);
                }
			}
            
            return;
        }

        void processInst (Instruction* Inst)
        {
            int iOpCode = Inst->getOpcode();  

            /* process by op code */
            switch (iOpCode)
            {
                case Instruction::Load:
				{
                    processLoad(Inst);
                    break;							
				}
				case Instruction::Store: 
                {
                    processStore(Inst);
                    break;							
				}
                case Instruction::Alloca:
                {
                    break;
                }
				default: 
				{
					processDefault(Inst);		
					break;
				}
            }

            return;
        }

        void VisitInstruction (Function &Func, BasicBlock & bBlock)
        {
            for (Instruction &Inst : bBlock) 
            {
                strMsg = "";
                addMsgField("caller:" + Func.getName().str());
                
                if (auto *CI = dyn_cast<CallInst>(&Inst)) 
                {
                    Function *CalleeFunc = CI->getCalledFunction();
                    if (CalleeFunc && CalleeFunc->hasName())
                    {
                        StringRef strCalleeName = CalleeFunc->getName();
                        if (strCalleeName.compare("llvm.dbg.declare"))
                        {                      
                            unsigned ArgNum = CI->getNumArgOperands();
                            while (ArgNum > 0)
                            {
                                Value* Arg = CI->getArgOperand(ArgNum-1);
                                addMsgField("use:" + getValueName(Arg));
                                ArgNum--;
                            }

                            addMsgField("callee:" + strCalleeName.str());
                        } 
                    }
                }
                
                else
                {
                    processInst (&Inst);
                }

                if (-1 != strMsg.find("define") || -1 != strMsg.find("use") || -1 != strMsg.find("callee"))
                {
                    //errs()<<Inst<<" \r\n";
                    //errs()<<"msg: "<< strMsg << " \r\n";
                    AddInstrumentation(Inst, strMsg);
                }
            }

            return;
        }

        void VisitBlock (Module *ptModule)
        {
            for (Module::iterator mIter = ptModule->begin(); mIter != ptModule->end(); mIter++) 
            {
                m_iVarNo = 0;
                Function &Func = *mIter;

                // no need to instrument
                if (!Func.getName().compare("charTojstring"))
                {
                    continue;
                }
                
                for (BasicBlock &bBlock : Func) 
                {
                    VisitInstruction (Func, bBlock);
                }
            }
            return;
        }
        
public:
        Instrument(Module *ptModule)
        {
            m_pModule = ptModule;
            m_iVarNo  = 0;
            m_valToNameMap.clear();
        } 

        void StartInstrumentation ()
        {
            VisitBlock (m_pModule);
        }
    };
    
	struct CollectionPass : public ModulePass 
	{
		static char ID;
    
		CollectionPass() : ModulePass(ID)
	    {
	    }
	    
	    StringRef getPassName() const override 
	    {
	       	return StringRef("CollectionPass");
	    }
	  	
	    bool runOnModule(Module &M) override 
	    {
            Instrument Inst (&M);

            Inst.StartInstrumentation();

            errs()<<"instrument " <<M.getName()<<"\r\n";
            
	    	return true;
	    }
	};
}

char CollectionPass::ID = 0;
static RegisterPass<CollectionPass> X("collection", "collect dynamic data flow information");

static void registerCTPass(const PassManagerBuilder &, legacy::PassManagerBase &PM) 
{
    PM.add(new CollectionPass());
}
                               
static RegisterStandardPasses RegisterCTPass(PassManagerBuilder::EP_EnabledOnOptLevel0, registerCTPass);

/* clang -Xclang -load -Xclang llvmCallTrace.so */



