#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>

#define ASSERT(x)\
{\
    if (!(x))\
    {\
	    printf ("Error: %s:%d \r\n", __FILE__, __LINE__);\
	    return -1;\
    }\
}

int InitConnection (char* pcIpAddr, int iPortNo)
{
    int iRet;
    int iLocalSk;
    struct sockaddr_in stAddr;

    iLocalSk = socket(AF_INET, SOCK_STREAM, 0);  
    ASSERT (iLocalSk > 0);

    memset(&stAddr, 0, sizeof(stAddr));
    stAddr.sin_family = AF_INET;
    stAddr.sin_port = htons(iPortNo);
    inet_pton(AF_INET, pcIpAddr, &stAddr.sin_addr);
    iRet = connect(iLocalSk, (struct sockaddr*)&stAddr, sizeof(stAddr));
    ASSERT(iRet >= 0);

    return iLocalSk;    
}

void CollectionInfo (char* pData)
{
    int iRet;
    
    int iLocalSk = InitConnection("192.168.159.131", 20008);
    if (iLocalSk <= 0)
    {
        printf ("socket is not inited successful \r\n");
        return;
    }

	/* send process result */
    iRet = send(iLocalSk, pData, strlen(pData), 0);
    printf ("send to server:[%u] %s \r\n", iRet, pData);
    
    return;   
}



