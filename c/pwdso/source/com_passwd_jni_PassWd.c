
#include "com_passwd_jni_PassWd.h"

#ifdef __cplusplus
extern "C" {
#endif

char* getPasswdFromUser (void);

jstring charTojstring(JNIEnv* env, const char* pat) 
{
    jclass strClass = (*env)->FindClass(env, "Ljava/lang/String;");

    jmethodID ctorID = (*env)->GetMethodID(env, strClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray bytes = (*env)->NewByteArray(env, strlen(pat));
    (*env)->SetByteArrayRegion(env, bytes, 0, strlen(pat), (jbyte*) pat);
    jstring encoding = (*env)->NewStringUTF(env, "GB2312");

    return (jstring) (*env)->NewObject(env, strClass, ctorID, bytes, encoding);
}

/*
 * Class:      
 * Method:    getPasswd
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_passwd_jni_PassWd_getIdInfo (JNIEnv *env, jobject obj)
{
	char *pwd;
	jstring jstrPwd;
	
	pwd = getPasswdFromUser ();
	
	jstrPwd = charTojstring(env, pwd);
	
	return jstrPwd;
}

#ifdef __cplusplus
}
#endif

