===============================================================
= program introduction
===============================================================
1. Function Description
a. The target program of analysis: Java program would call JNI api to get sensitive information, which flows through c module to java module 
                                   and get leaked in java module.
b. the analysis program:
b1. analysis server: implement analysis online at back end and produce complete taint flow paths
b2. instrumentation: add instrumentation (Java by Soot and C by llvm) to program and send run-time information to analysis server

2. Directory Description
a simple demo for code analysis in program which is implemented by multi-languages(c & java in this program)
multi-lang
├─analysisServer         -------     analysis server: receive run-time message and analyze it online
├─c                      -------     c language part
│  ├─instrument          -------     the information collection client which would be instrumented into pwdso 
│  └─pwdso               -------     the JNI dynamic library, as a part of target program
├─java                   -------     java language part
│  └─src                 
│      ├─com             -------     java program, cal JNI API implemtented by "pwdso", as a part of target program
│      ├─instrument      -------     the information collection client which would be instrumented into "com"
│      └─net             -------     java net communication module
└─llvm
    └─Collection         -------     llvm pass, which would add instrumentation to "pwdso" while compiling
	
3. Run the program
------------------
| Analysis server|
------------------
cd analysisServer; make clean; make

| C part|
---------
a. build llvm pass, add Collection to llvm-7.0.0.src/lib/Transforms
   generate dynamic library "llvmCollection.so" and copy it to /usr/lib
b. cd c/instrument; make clean; make
c. cd c/pwdso; make clean make

------------
| Java part|
------------
a. build the java project to make sure that all class files created
   cd java; mkdir instrument; cd instrument; ln -s ../bin/com
b. run the instrument java program with Soot to generate sootOutput
c. cd sootOutput; java -Djava.library.path='../../c/pwdso/'  com.passwd.jni.Main

4. Run result
===============================================
==                analysis Server            ==
===============================================

[c]getPasswdFromUser:Inst11-{define:curPwd use:%0}
[c]getPasswdFromUser:Inst12-{ use:curPwd}
[c]getPasswdFromUser:Inst13-{ use:curPwd}
[c]Java_com_passwd_jni_PassWd_getIdInfo:Inst14-{define:pwd use:call}
[c]Java_com_passwd_jni_PassWd_getIdInfo:Inst16-{ use:pwd}
[c]Java_com_passwd_jni_PassWd_getIdInfo:Inst17-{ use:env.addr use:pwd callee:charTojstring}
[c]Java_com_passwd_jni_PassWd_getIdInfo:Inst18-{define:jstrPwd use:call1}
[c]Java_com_passwd_jni_PassWd_getIdInfo:Inst19-{ use:jstrPwd}
[c]Java_com_passwd_jni_PassWd_getIdInfo:Inst20-{ use:jstrPwd}
[java]<com.passwd.jni.PassWd: boolean authenticate()>:Inst21-{define:r3 use:r0 callee:<com.passwd.jni.PassWd: java.lang.String getIdInfo()>}
[java]<com.passwd.jni.PassWd: boolean authenticate()>:Inst24-{ use:r3 use:$r1 callee:<java.io.Writer: void write(java.lang.String)>}
[java]<com.passwd.jni.PassWd: boolean authenticate()>:Inst28-{define:$z1 use:r3 use:r0 callee:<com.passwd.jni.PassWd: boolean doAuthentiate(java.lang.String)>}

   
