#ifndef __MACRO_H__
#define __MACRO_H__

#define SIZE_QUEUE    (1024)
#define SIZE_MSGLEN   (256)

#define SIZE_CGNODE   (128)
#define SIZE_INSTNODE (1024)

#define SIZE_USEVAL   (4)

#define SIZE_FUNCNAME (128)


#define SIZE_DDNODE   (256)


#endif