#ifndef __SERVER_H__
#define __SERVER_H__



#define ASSERT(x)\
{\
    if (!(x))\
    {\
	    printf ("Error: %s:%d \r\n", __FILE__, __LINE__);\
	    return -1;\
    }\
}


typedef void (*DATA_PROCESS) (char* data);

typedef struct tag_ServerCfg
{
    int iPort;
    DATA_PROCESS dataProcess;
}ServerCfg;

int ServerThread (ServerCfg* pstScfg);



#endif