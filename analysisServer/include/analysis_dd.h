#ifndef __ANALYSIS_DD_H__
#define __ANALYSIS_DD_H__
#include "analysis.h"

typedef struct tag_DDtable
{
    DDnode ddNode[SIZE_DDNODE];
    int    iddNodeIndex;
}DDtable;

#endif