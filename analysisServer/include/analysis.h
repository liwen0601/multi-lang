#ifndef __ANALYSIS_H__
#define __ANALYSIS_H__

#include "macro.h"

typedef struct tag_InstInfoQueue
{
    char qInstInfo[SIZE_QUEUE][SIZE_MSGLEN];
    int iInIndex;
    int iOutIndex;
}InstInfoQueue;

typedef struct tag_Value
{
    char VName[SIZE_FUNCNAME];
}Value;

typedef struct tag_InstNode
{
    int InstNo;

    Value Lang;
    
    Value DefVal;
    
    Value UseVal[SIZE_USEVAL];
    int UseValNum;

    Value Callee;

    struct tag_InstNode* nxtInstNode;
}InstNode;

typedef struct tag_DDnode
{
	InstNode *DefInst;
	InstNode *UseInst;

    struct tag_DDnode* nxtDDNode;
}DDnode;

typedef struct tag_DynCGnode
{
    char FuncName[128];
    InstNode* InstHdr;
    InstNode* InstTail;
    int InstNum;
    int Depth;

    DDnode *ddNodeHdr;
    DDnode *ddNodeTail;
}DynCGnode;

typedef struct tag_DynCG
{
    DynCGnode CGNode[SIZE_CGNODE];
    int RevNum;
    int NodeNum;

    InstNode InstNodeBuf[SIZE_INSTNODE];
    int InstIndex;

}DynCG;

typedef struct tag_TaintNode
{
    DynCGnode *pCGNode;
    InstNode* Inst;
    
    struct tag_TaintNode* nxtTainNode;    
}TaintNode;

typedef struct tag_AnzState
{
    int LastDepth;
    InstNode *LastInst;
    InstNode *CurInst;
    
    DDnode* ddNode[SIZE_USEVAL];

    TaintNode* curTainHdr;
    TaintNode* curTainTail;
    TaintNode* TaintSrc;
}AnzState;


//=======================================================================================================
DynCG* GetDynCG ();
InstNode* GetPreInst (DynCGnode *pCGNode, InstNode *CurInst);

#endif