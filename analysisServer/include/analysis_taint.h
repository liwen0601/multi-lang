#ifndef __ANALYSIS_TAINT_H__
#define __ANALYSIS_TAINT_H__
#include "analysis.h"

typedef struct tag_Tainttable
{
    TaintNode taintNode[SIZE_INSTNODE];
    int taintIndex;
}Tainttable;

#endif