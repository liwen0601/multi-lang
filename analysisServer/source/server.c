#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include "server.h"

static int g_socket = 0;

int BindToPort (int iPortNo, int icurSk, struct sockaddr_in* psAddr)
{
    int iRet;

	psAddr->sin_family = AF_INET; 
    psAddr->sin_addr.s_addr = htons(INADDR_ANY);
	psAddr->sin_port =	htons(iPortNo);
	iRet = bind(icurSk, (struct sockaddr*)psAddr, sizeof(struct sockaddr_in));
	ASSERT(iRet >= 0);

	return 0;
}

int InitServer (int iPortNo, struct sockaddr_in* psAddr)
{
    int iRet;
    int iLocalSk;  

    iLocalSk = socket(AF_INET, SOCK_STREAM, 0);  
    ASSERT (iLocalSk > 0);
    
    iRet = BindToPort(iPortNo, iLocalSk, psAddr);
    ASSERT(iRet >= 0);
    
    iRet = listen (iLocalSk, 1);
    ASSERT(iRet >= 0);

    return iLocalSk;    
}

int ServerThread (ServerCfg *pstCfg)
{
	int iRet;
    int iLocalSk, iCnnSk;
	struct sockaddr_in sAddr;  
    socklen_t size;
	char InBuf[256];
    
    iLocalSk = InitServer(pstCfg->iPort, &sAddr);
    ASSERT(iLocalSk > 0);
    g_socket = iLocalSk;

    printf ("===============================================\r\n");
    printf ("==                analysis Server            ==\r\n");    
    printf ("===============================================\r\n\r\n");
	while (1)
	{
	    size = sizeof(sAddr);
	    iCnnSk = accept(iLocalSk, (struct sockaddr*)&sAddr, &size);
	    ASSERT(iCnnSk > 0);
	    
	    memset (InBuf, 0, sizeof(InBuf));
			
	    /* recv from client */
	 	iRet = recv(iCnnSk, InBuf, sizeof(InBuf), 0);
        ASSERT(iRet >= 0)
		//printf ("%s \r\n", InBuf);

        if (pstCfg->dataProcess)
        {
            pstCfg->dataProcess(InBuf);
        }
	}
	
    close(iLocalSk);
    return 0;   
}

void SignalINTProc(int sig) 
{
    if (g_socket != 0)
    {
        close(g_socket);
    }

    void PrintDCG();

    PrintDCG();

    exit (0);
}


