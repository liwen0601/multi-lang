
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "analysis.h"

static InstInfoQueue queue;
static DynCG dynCG;

////////////////////////////////////////////////////////////////////////
void TaintAnalysis (DynCGnode *pCGNode, AnzState *anzState);
void DynDdAnalysis (DynCGnode *pCGNode, AnzState *anzState);
void PrintCaller(DynCGnode *pCGNode, AnzState *anzState);
////////////////////////////////////////////////////////////////////////

DynCG* GetDynCG ()
{
    return &dynCG;
}

int isFull(InstInfoQueue *queue)
{
    if ((queue->iInIndex+1)%SIZE_QUEUE == queue->iOutIndex)
    {
        return 1;
    }

    return 0;
}

int isEmpty(InstInfoQueue *queue)
{
    if (queue->iInIndex == queue->iOutIndex)
    {
        return 1;
    }

    return 0;
}


void pushQueue(char* pInstInfo)
{
    int iMsgLen;
    int iInIndex;
    
    if (isFull(&queue))
    {
        printf("isFull(&queue)!!!!\r\n");
        return;
    }

    iMsgLen = strlen(pInstInfo);
    if (iMsgLen >= SIZE_MSGLEN)
    {
        printf("iMsgLen >= SIZE_MSGLEN!!!!!!\r\n");
        return;
    }
    
    iInIndex = queue.iInIndex;
    queue.iInIndex = (queue.iInIndex+1)%SIZE_QUEUE;
    
    strncpy (queue.qInstInfo[iInIndex], pInstInfo, iMsgLen);
    
    //printf("***push, no:%d/%d ---> %s \r\n", 
    //    iInIndex, (queue.iInIndex+SIZE_QUEUE-queue.iOutIndex)%SIZE_QUEUE, queue.qInstInfo[iInIndex]);
    
    return;
}

char* pullQueue()
{
    int iOutIndex;
    
    if (isEmpty(&queue))
    {
        return NULL;
    }

    iOutIndex = queue.iOutIndex;
    queue.iOutIndex = (queue.iOutIndex+1)%SIZE_QUEUE;

    //printf("***pull, no:%d/%d ---> %s\r\n", 
    //       iOutIndex, (queue.iInIndex+SIZE_QUEUE-queue.iOutIndex)%SIZE_QUEUE, queue.qInstInfo[iOutIndex]);
        
    return queue.qInstInfo[iOutIndex];
}

char* GetTypeValue(char* pMsg, char* Type, char* Value)
{
    if (*pMsg++ != '{')
    {
        return NULL;
    }

    /* type */
    while (*pMsg != 0 && *pMsg != ':')
    {
        *Type++ = *pMsg++;
    }

    if (*pMsg++ != ':')
    {
        return NULL;
    }

    /* value */
    while (*pMsg != 0 && *pMsg != '}')
    {
        *Value++ = *pMsg++;
    }

    if (*pMsg++ != '}')
    {
        return NULL;
    }

    return pMsg; 
}

DynCGnode* NewCGNode(char* CallerName, int LastDepth)
{
    DynCG *dCG = GetDynCG();
    DynCGnode *pCGNode;
   
    if (dCG->NodeNum >= SIZE_CGNODE)
    {
        return NULL;
    }

    pCGNode = dCG->CGNode + dCG->NodeNum;
    if (dCG->NodeNum == dCG->RevNum)
    {
        pCGNode->Depth = 0;
    }
    else
    {
        pCGNode->Depth = LastDepth + 1;
    }
    dCG->NodeNum++;
     
    strncpy (pCGNode->FuncName, CallerName, sizeof(pCGNode->FuncName));

    //printf ("new cgNode: [%d]%s \r\n", pCGNode->Depth, pCGNode->FuncName);

    return pCGNode;
}

DynCGnode* GetCGNode(char* CallerName)
{
    DynCG *dCG = GetDynCG();
    int Index = dCG->RevNum;
    DynCGnode *pCGNode = dCG->CGNode + Index;

    while (Index < dCG->NodeNum)
    {
        if (!strcmp(pCGNode->FuncName, CallerName))
        {
            return pCGNode;
        }

        Index++;
        pCGNode++;
    }

    return NULL;
}

DynCGnode* GetOrNewCGNode(char* CallerName, AnzState *anzState)
{
    DynCGnode *pCGNode = GetCGNode(CallerName);

    if (pCGNode == NULL)
    {
        pCGNode = NewCGNode(CallerName, anzState->LastDepth);
    }

    return pCGNode;
}

InstNode* NewInstNode(InstNode *instNode)
{
    DynCG *dCG = GetDynCG();
    InstNode* newNode;

    if (dCG->InstIndex >= SIZE_INSTNODE)
    {
        return NULL;
    }

    newNode = dCG->InstNodeBuf + dCG->InstIndex;
    memcpy (newNode, instNode, sizeof(InstNode));
    newNode->InstNo = dCG->InstIndex;

    dCG->InstIndex++;
    
    return newNode;
}

InstNode* GetPreInst (DynCGnode *pCGNode, InstNode *CurInst)
{
    InstNode *PreInst = pCGNode->InstHdr;

    while (PreInst != NULL)
    {
        if (PreInst->nxtInstNode == CurInst)
        {
            return PreInst;
        }

        PreInst = PreInst->nxtInstNode;
    }

    return PreInst;
}


void UpdateNode (DynCGnode *pCGNode, InstNode *instNode)
{
    int UseNo;
    InstNode *inst;
    InstNode *InstAry[SIZE_USEVAL];
    
    // merge two insts
    if (!strcmp(instNode->Lang.VName, "c") && instNode->Callee.VName[0] != 0 && instNode->UseValNum != 0)
    {
        inst = pCGNode->InstHdr;
        UseNo = 0;
        while (inst != NULL)
        {
            InstAry[UseNo] = inst;

            UseNo = (UseNo+1)%instNode->UseValNum;
            inst = inst->nxtInstNode;
        }
        
        for (UseNo = 0; UseNo < instNode->UseValNum; UseNo++)
        {
            strcpy (instNode->UseVal[UseNo].VName, InstAry[UseNo]->UseVal[0].VName);
        }
        
        return;
    }

    
}


void AddInstNodeToCGNode(DynCGnode *pCGNode, InstNode *instNode)
{
    InstNode* InstTail;
    InstNode* newNode;

    UpdateNode(pCGNode, instNode);
   
    // create new node
    newNode = NewInstNode(instNode);
    if (newNode == NULL)
    {
        return;
    }
    newNode->nxtInstNode = NULL;
    
    if (pCGNode->InstHdr == NULL)
    {
        pCGNode->InstHdr  = newNode;
        pCGNode->InstTail = newNode;
        pCGNode->InstNum = 1;
    }
    else
    {
        pCGNode->InstTail->nxtInstNode = newNode;
        pCGNode->InstTail = newNode;
        pCGNode->InstNum++;
    }

    return;
}


DynCGnode* DecodeMsg(char* pMsg, AnzState *anzState)
{
    char* pData = pMsg;
    char Type[16];
    char Value[SIZE_FUNCNAME];
    InstNode instNode;
    DynCGnode *pCGNode;

    memset (&instNode, 0, sizeof(instNode));
    while (*pData != 0)
    {
        memset(Type, 0, sizeof(Type));
        memset(Value, 0, sizeof(Value));
        
        pData = GetTypeValue(pData, Type, Value);
        if (pData == NULL)
        {
            return NULL;
        }

        if (!strcmp(Type, "caller"))
        {
            pCGNode = GetOrNewCGNode(Value, anzState);
            if (pCGNode == NULL)
            {
                return NULL;
            }
        }
        else if (!strcmp(Type, "callee"))
        {
            strncpy(instNode.Callee.VName, Value, sizeof(instNode.Callee.VName));
        }
        else if (!strcmp(Type, "define"))
        {
            strncpy(instNode.DefVal.VName, Value, sizeof(instNode.DefVal.VName));
        }
        else if (!strcmp(Type, "use"))
        {
            if (instNode.UseValNum >= SIZE_USEVAL)
            {
                return NULL;
            }
            strncpy(instNode.UseVal[instNode.UseValNum].VName, 
                    Value, sizeof(instNode.UseVal[instNode.UseValNum].VName));
            instNode.UseValNum++;
        }
        else if (!strcmp(Type, "lang"))
        {
            strncpy(instNode.Lang.VName, Value, sizeof(instNode.Lang.VName));
        }
        else
        {
            printf("***** type - %s : value - %s \r\n", Type, Value);
            continue;
        }
    }

    if (pCGNode == NULL)
    {
         return NULL;
    }

    AddInstNodeToCGNode(pCGNode, &instNode);

    anzState->CurInst = pCGNode->InstTail;

    return pCGNode;
}

void AnalyzeMsg (DynCGnode *pCGNode, AnzState *anzState)
{   
    DynDdAnalysis (pCGNode, anzState);

    TaintAnalysis(pCGNode, anzState);

    //PrintCaller(pCGNode, anzState);
    
    return;
}

int Analyze (void)
{
    char* pMsg;
    DynCGnode *pCGNode;
    AnzState anzState = {0};

    anzState.LastDepth = -1;
    while (1)
    {
        pMsg = pullQueue();
        if (pMsg == NULL)
        {
            //printf("there is no message in the queue now, sleep and wait..\r\n");
            sleep(1);   
            continue;
        }

        pCGNode = DecodeMsg(pMsg, &anzState);
        if (pCGNode == NULL)
        {
            return 0 ;
        }

        AnalyzeMsg(pCGNode, &anzState);

        anzState.LastDepth = pCGNode->Depth;
        anzState.LastInst  = anzState.CurInst;
        memset (anzState.ddNode, 0, sizeof(anzState.ddNode));
    }


	return 0;
}

int setAnalysisSrc (char * Source)
{
    DynCGnode *pdAnSrc;
    DynCG* dCG = GetDynCG ();   
    AnzState anzState = {0};

    pdAnSrc = DecodeMsg(Source, &anzState);
    if (pdAnSrc == NULL)
    {
        return -1 ;
    }

    dCG->RevNum++;
    return 0;
}
