#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include "server.h"


int Analyze (void);
void pushQueue(char* pInstInfo);
void SignalINTProc(int sig);
int setAnalysisSrc (char * Source);


int main (int argc,char *argv[])
{
    int ret; 
    pthread_t t1;
    static ServerCfg cfg = {20008, pushQueue};

    signal(SIGINT, SignalINTProc);

    //{caller:getPasswdFromUser}{define:curPwd}{use:%0}
    if (setAnalysisSrc ("{caller:getPasswdFromUser}{define:curPwd}") != 0)
    {
        printf ("set analysis source fail!!! \r\n");
        return -1;
    }
           
    /*创建线程一*/
    ret = pthread_create(&t1, NULL, (void*)ServerThread, &cfg);
    if(ret != 0) 
    {
        printf("Create pthread error!\n");
        return -1;
    }

    Analyze();

    return 0;
}




