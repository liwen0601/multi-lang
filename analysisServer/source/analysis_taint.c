#include <stdio.h>
#include <string.h>
#include "analysis_taint.h"

static Tainttable TTtable;

void PrintTaint (TaintNode *ttNode);

DynCGnode* GetSrcCgNode ()
{
    DynCG *dynCG = GetDynCG();

    return dynCG->CGNode;
}


int IsReachTaintSrc(DynCGnode *pCGNode, InstNode* CurInst)
{
    DynCGnode *srcCgNode = GetSrcCgNode();
    InstNode* TaintInst;

    if (strcmp(srcCgNode->FuncName, pCGNode->FuncName))
    {
        return 0;
    }

    TaintInst = srcCgNode->InstHdr;
    if (TaintInst == NULL)
    {
        return 0;
    }
    
    if (strcmp(TaintInst->DefVal.VName, CurInst->DefVal.VName))
    {
        return 0;
    }

    return 1;
}

int IsDataDependent (DynCGnode *pCGNode, InstNode* TaintNode, InstNode* CurInst)
{
    DDnode *ddNode = pCGNode->ddNodeHdr;

    while (ddNode != NULL)
    {
        if (TaintNode == ddNode->DefInst &&
            CurInst   == ddNode->UseInst)
        {
            return 1;
        }
            
        ddNode = ddNode->nxtDDNode;
    }
    
    return 0;
}

TaintNode* NewTaintNode (DynCGnode *pCGNode, InstNode* CurInst, AnzState *anzState)
{
    TaintNode *ttNode;
    Tainttable* ttTable = &TTtable;

    if (ttTable->taintIndex >= SIZE_INSTNODE)
    {
        return NULL;
    }

    ttNode = ttTable->taintNode + ttTable->taintIndex++;
    ttNode->Inst    = CurInst;
    ttNode->pCGNode = pCGNode;
    ttNode->nxtTainNode = NULL;

    if (anzState->curTainHdr == NULL)
    {
        anzState->curTainHdr = ttNode;
        anzState->curTainTail = ttNode;
    }
    else
    {
        anzState->curTainTail->nxtTainNode = ttNode;
        anzState->curTainTail = ttNode;
    }

    return ttNode;
}

int IsLangType (InstNode *inst, char *Type)
{
    return (!strcmp(inst->Lang.VName, Type));
}

void TaintAnalysis (DynCGnode *pCGNode, AnzState *anzState)
{
    int iUse;
    InstNode* CurInst;
    TaintNode* LastTtNode;
    TaintNode* ttNode = NULL;

    CurInst = anzState->CurInst;

    // if entry the taint source, start analysis
    if (anzState->curTainHdr == NULL)
    {
        if (!IsReachTaintSrc(pCGNode, CurInst))
        {
            return;
        }

        ttNode = NewTaintNode(pCGNode,CurInst, anzState);
        if (ttNode == NULL)
        {
            return;
        }    

        anzState->TaintSrc = ttNode;
        PrintTaint (ttNode);
    }
    else
    {
        LastTtNode = anzState->curTainTail;

        // int the same function
        if (!strcmp (LastTtNode->pCGNode->FuncName, pCGNode->FuncName))
        {
            // if exist data dependence
            if (!IsDataDependent(pCGNode, anzState->TaintSrc->Inst, CurInst))
            {
                if (CurInst->DefVal.VName[0] == 0 || 
                    strncmp (CurInst->UseVal[0].VName, "call", sizeof("call")-1) ||
                    !IsDataDependent(pCGNode, anzState->TaintSrc->Inst, anzState->LastInst))
                {
                    return;
                }
            }

            ttNode = NewTaintNode(pCGNode, CurInst, anzState);
            if (ttNode == NULL)
            {
                return;
            }

            if (CurInst->DefVal.VName[0] != 0)
            {
                anzState->TaintSrc = ttNode;
            }

            PrintTaint (ttNode);
        }
        else
        {
            // if current inst define a val through call, the return value is the taint point
            if (CurInst->DefVal.VName[0] != 0)
            {
                if (IsLangType(CurInst, "c"))
                {
                    if (strncmp (CurInst->UseVal[0].VName, "call", 4))
                    {
                        return;
                    }

                    InstNode *PreInst = GetPreInst(pCGNode, CurInst);
                    if (PreInst == NULL)
                    {
                        return;
                    }

                    if (strcmp (PreInst->Callee.VName, LastTtNode->pCGNode->FuncName))
                    {
                        return;
                    }
                }

                if (IsLangType(CurInst, "java"))
                {
                    // jni call
                    if (IsLangType(LastTtNode->Inst, "c"))
                    {
                        if (CurInst->Callee.VName[0] == 0 ||
                            strncmp (LastTtNode->pCGNode->FuncName, "Java_", 5))
                        {
                            return;
                        }
                    }
                    else
                    {
                        //printf("**********unsupported analysis!!!\r\n");
                        return;                        
                    }
                }
                
                
                ttNode = NewTaintNode(pCGNode, CurInst, anzState);
                if (ttNode == NULL)
                {
                    return;
                }

                // need to update the taint source
                anzState->TaintSrc = ttNode;

                PrintTaint (ttNode);
            }
        }
    }
    
    return;
    
}




