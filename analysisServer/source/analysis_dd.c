#include <stdio.h>
#include <string.h>
#include "analysis_dd.h"

static DDtable ddTable;

InstNode* GetDefInstNode(DynCGnode *pCGNode, Value *UseVal)
{
    InstNode* DefInst = NULL;
    InstNode* inst = pCGNode->InstHdr;

    while (inst != NULL)
    {
        if (!strcmp(inst->DefVal.VName, UseVal->VName))
        {
            DefInst = inst; 
        }

        inst = inst->nxtInstNode;
    }

    return DefInst;
}

DDnode* NewDDNode ()
{
    DDnode* ddNode;
    DDtable *ddt = &ddTable;

    if (ddt->iddNodeIndex >= SIZE_DDNODE)
    {
        return NULL;
    }

    ddNode = ddt->ddNode + ddt->iddNodeIndex++;

    return ddNode;
}

void DynDdAnalysis (DynCGnode *pCGNode, AnzState *anzState)
{
    int iUse;
    InstNode* CurInst;
    InstNode* DefInst;
    DDnode* ddNode;

    CurInst = anzState->CurInst;

    // cal def-use
    if (CurInst->UseValNum == 0)
    {
        return;
    }

    iUse = 0;
    while (iUse < CurInst->UseValNum)
    {
        DefInst = GetDefInstNode(pCGNode, CurInst->UseVal+iUse);
        if (DefInst == NULL)
        {
            return;
        }

        // new ddnode
        ddNode = NewDDNode ();
        if (ddNode == NULL)
        {
            return;
        }

        ddNode->DefInst = DefInst;
        ddNode->UseInst = CurInst;
        ddNode->nxtDDNode = NULL;

        // add to list
        if (pCGNode->ddNodeHdr == NULL)
        {
            pCGNode->ddNodeHdr  = ddNode;
            pCGNode->ddNodeTail = ddNode;
        }
        else
        {
            pCGNode->ddNodeTail->nxtDDNode = ddNode;
            pCGNode->ddNodeTail = ddNode;
        }
        
        anzState->ddNode[iUse] = ddNode;
        iUse++;
    }
    
    return;
    
}




