
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "analysis.h"

DynCG* GetDynCG ();


void PrintInst(InstNode   *instNode)
{  
    printf("Inst%d-{", instNode->InstNo);
    if (instNode->DefVal.VName[0] != 0)
    {
        printf("define:%s", instNode->DefVal.VName);
    }
                  
    int UseNum = 0;
    while (UseNum < instNode->UseValNum)
    {
        printf(" use:%s", instNode->UseVal[UseNum].VName);
        UseNum++;
    }

    if (instNode->Callee.VName[0] != 0)
    {
        printf(" callee:%s", instNode->Callee.VName);
    }
    
    printf("}");
}

void PrintDynDDNode (DDnode* ddNode)
{
    PrintInst(ddNode->DefInst);

    printf (" <---> ");

    PrintInst(ddNode->UseInst);

    printf (" \r\n");
}

void PrintDynDD(DynCGnode *pCGNode)
{
    DDnode* ddNode = pCGNode->ddNodeHdr;

    while (ddNode != NULL)
    {
        PrintDynDDNode (ddNode);

        ddNode = ddNode->nxtDDNode;
    }

    return;
}


void PrintDCG()
{
    DynCG *dCG = GetDynCG();
    int Index = dCG->RevNum;
    InstNode  *instNode;
    DynCGnode *pCGNode;

    pCGNode = dCG->CGNode + Index;
    while (Index < dCG->NodeNum)
    {
        printf("============================================\r\n");
        printf("Function: %s \r\n", pCGNode->FuncName);
        instNode = pCGNode->InstHdr;
        while (instNode != NULL)
        {
            PrintInst (instNode);
            printf("\r\n");

            instNode = instNode->nxtInstNode;
        }

        PrintDynDD (pCGNode);

        printf("============================================\r\n\r\n");

        Index++;
        pCGNode++; 
    }
}

void PrintTab(int Num)
{
    while (Num > 0)
    {
        printf ("\t");
        Num--;
    }
}

void PrintCaller(DynCGnode *pCGNode, AnzState *anzState)
{
    int ddNum = 0;
    DynCG *dCG = GetDynCG();

    if (pCGNode->Depth != anzState->LastDepth)
    {
        printf("\r\n");
        PrintTab(pCGNode->Depth);       
        printf ("[%d]%s \r\n", pCGNode->Depth, pCGNode->FuncName);
    }

    while (anzState->ddNode[ddNum] != NULL && ddNum < SIZE_USEVAL)
    {
        PrintTab(pCGNode->Depth);
        PrintDynDDNode (anzState->ddNode[ddNum]);

        ddNum++;
    }
}

void PrintTaint (TaintNode *ttNode)
{
    printf("[%s]%s:", ttNode->Inst->Lang.VName, ttNode->pCGNode->FuncName);
    PrintInst(ttNode->Inst);
    printf("\r\n");
}