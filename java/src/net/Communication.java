package net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class Communication
{
	Socket m_stSocket;
	String m_strAddr;
	int    m_iPort;
	
	String m_strOut;
	
	public Communication (String strAddr, int port)
	{
		m_stSocket = null;
		m_strAddr  = strAddr;
		m_iPort    = port;
		
		m_strOut   = null;
	}
	
	public Communication ()
	{
		m_stSocket = null;
		m_strAddr  = "localhost";
		m_iPort    = 18888;
		
		m_strOut   = null;
	}

	 public String SendAndRecvData(String In) throws IOException 
	 {		
		/* init before send, and deinit after receive*/
		InetAddress inetAddr = InetAddress.getByName(m_strAddr);
		m_stSocket = new Socket(inetAddr, m_iPort);	
			
		PrintWriter pw  = new PrintWriter(m_stSocket.getOutputStream());
		BufferedReader br = new BufferedReader(new InputStreamReader(m_stSocket.getInputStream()));
					  
		pw.write(In);
		pw.flush();
	    System.out.println ("In: " + In);
	        
	    m_strOut = br.readLine();
	    System.out.println ("Out: " + m_strOut);
	    
	    pw.close();
	    br.close();
	    m_stSocket.close();
	    
	    return m_strOut;
	 }
	 
	 public void SendData(String In) throws IOException 
	 {		
		/* init before send, and deinit after receive*/
		InetAddress inetAddr = InetAddress.getByName(m_strAddr);
		m_stSocket = new Socket(inetAddr, m_iPort);	
			
		PrintWriter pw  = new PrintWriter(m_stSocket.getOutputStream());			  
		pw.write(In);
		pw.flush();
	    //System.out.println ("In: " + In);
	    
	    pw.close();
	    m_stSocket.close();
	    
	    return;
	 }
}
