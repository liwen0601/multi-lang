package instrument;

import java.util.Iterator;
import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Value;
import soot.ValueBox;
import soot.jimple.Jimple;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.tagkit.LineNumberTag;
import soot.util.Chain;

public class Instrument extends BodyTransformer
{
	static SootClass  Collection = Scene.v().loadClassAndSupport("instrument.Collection");	
	static SootMethod SendInfo   = Collection.getMethodByName("SendInfo");
	
	protected String addField(String strField)
	{
		return "{" + strField + "}";		
	}
	
	protected String getDefUseValue (String strTag, Iterator bi, String strValInfo)
	{		
		while (bi.hasNext()) 
		{
			Object o = bi.next();
			if (o instanceof ValueBox) 
			{
				Value v = ((ValueBox)o).getValue();
				if (v instanceof Local)
				{
					strValInfo += addField(strTag + v.toString()/* + " type:" + v.getType().toString()*/);					
				}		
			}
		}
		
		return strValInfo;
	}
	
	/*
	 * iterate current unit, insert trace statement before and after the invoke statement
	 * @see soot.BodyTransformer#internalTransform(soot.Body, java.lang.String, java.util.Map)
	 */
	protected void internalTransform(Body body, String phaseName, Map<String, String> options)
	{
		Stmt dynStmt;
		SootMethod caller = body.getMethod();
		
		// exception
		if (caller.getSignature().contains("void <init>()"))
		{
		     return;
		}
		
		System.out.println(caller.getSignature() + ": ");

		// get body's unit as a chain
		Chain units = body.getUnits();

		// get a snapshot iterator of the unit since we are going to
		// mutate the chain when iterating over it.
		//
		Iterator<Stmt> stmtIt = units.snapshotIterator();
		while (stmtIt.hasNext())
		{
			String strValInfo = "";
			Iterator it;
			Stmt stmt = (Stmt) stmtIt.next();
			
			// find definitions
			it = stmt.getDefBoxes().iterator();
			strValInfo = getDefUseValue("define:", it, strValInfo);
			
			// find uses
			it = stmt.getUseBoxes().iterator();
			strValInfo = getDefUseValue("use:", it, strValInfo);
			
			// if current statement has no variable define or use, no need to process
			if (strValInfo.isEmpty())
			{
				continue;
			}
			
			// add caller information
			strValInfo += addField("caller:" + caller.getSignature());
			
			// invoke expression
			if (stmt.containsInvokeExpr())
			{
				SootMethod callee = stmt.getInvokeExpr().getMethod();
				strValInfo += addField("callee:" + callee.getSignature());
			}
			
			// add lang type
			strValInfo += addField("lang:java");

			System.out.println (strValInfo);

			// add instrument for information collection
			dynStmt = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(SendInfo.makeRef(), 
                                                                              StringConstant.v(strValInfo)
                                                                             )
                                              );
            units.insertAfter(dynStmt, stmt);
		}
	}
}
