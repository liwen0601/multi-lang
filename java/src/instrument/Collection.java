package instrument;

import java.io.IOException;

import net.Communication;

public class Collection
{
	static int iMsgNo = 0;
	
	public static void SendInfo (String strInfo)
	{
		String strMsg;
		Communication cmm = new Communication("localhost", 20008);
		
		try
		{
			//strMsg = "{msg_id:" + Integer.toString(iMsgNo) + "} " + strInfo;
			strMsg = strInfo;
			cmm.SendData(strMsg);
			System.out.println(strMsg);
			
			iMsgNo++;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}
}
