package instrument;

import java.util.ArrayList;
import java.util.List;

import soot.PackManager;
import soot.Scene;
import soot.Transform;
import soot.Transformer;
import soot.options.Options;

public class Main
{
	private void initSoot (String strClassPath, String strCheckPath)
	{
		List<String> lstCheckPath = new ArrayList<String>();
		
		lstCheckPath.add(strCheckPath);	
		Options.v().set_process_dir(lstCheckPath);	
		
		if (!strClassPath.isEmpty())
		{	
			Options.v().set_soot_classpath(Scene.v().defaultClassPath() + ";" + strClassPath);
		}
		
		Options.v().set_output_format(Options.output_format_class);
		//Options.v().set_output_format(Options.output_format_jimple);
		
		System.out.println ("soot class path: " + Scene.v().getSootClassPath());	
	}
	
	private void runSoot (Object oTransObj, String strPhaseName)
	{
		PackManager.v().getPack("jtp").add(new Transform(strPhaseName, (Transformer) oTransObj));
		
		System.out.println ("start load class...");
		
		Scene.v().loadNecessaryClasses();
		
		System.out.println ("start run packs...");
	    PackManager.v().runPacks();
	    
	    System.out.println ("start output...");
		
		/* write jimple */
		PackManager.v().writeOutput();
		
		return;
	}
	
	public static void main(String[] args)
	{
		String clPath   = "Y:\\multi-lang\\java\\bin";
		String testPath = "Y:\\multi-lang\\java\\instrument";
		
		Main myMain = new Main();
		
		myMain.initSoot(clPath, testPath);
		
		myMain.runSoot(new Instrument(), "jtp.cllt");
		
		System.out.println ("finished!");

	}
}
