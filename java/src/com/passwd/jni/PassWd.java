package com.passwd.jni;

import java.io.FileWriter;
import java.io.IOException;

public class PassWd
{
	static String strPwdString = "default";
	
	static
	{
        System.loadLibrary("PassWdJni");
    }

    public PassWd() 
    {
    }
    
    public native String getIdInfo();
    
    private boolean doAuthentiate (String strpwd)
    {
        return true;
    }
    
    public boolean authenticate()
    {
    	String strPwd = strPwdString;
    	
    	if (strPwd.equals("default"))
        {
    		strPwd = getIdInfo();
    	}
            
    	try
		{
    		FileWriter writer = new FileWriter("pwd.txt", false);  
            writer.write(strPwd);  
            writer.close(); 
            
            // authenticate
            return doAuthentiate(strPwd);
		}		
	    catch (IOException e)
		{
			e.printStackTrace();
		} 
    	
    	return false;
    }
}
